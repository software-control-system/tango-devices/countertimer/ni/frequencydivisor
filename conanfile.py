from conan import ConanFile

class FrequencyDivisorRecipe(ConanFile):
    name = "frequencydivisor"
    executable = "ds_FrequencyDivisor"
    version = "1.0.9"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Gwenaelle Abeille"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/countertimer/ni/frequencydivisor.git"
    description = "FrequencyDivisor device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("cpptango/9.2.5@soleil/stable")
        self.requires("ni660xsl/[>=1.0]@soleil/stable")
        self.requires("nidaqmx/[>=1.0]@soleil/stable")
        self.requires("ace/5.7.0@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
